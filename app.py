from flask import Flask, request, redirect, render_template
from pymongo import MongoClient

app = Flask(__name__)
client = MongoClient("mongodb://127.0.0.1:27017")

db = client.customersapp
col = db["customers"]


@app.route("/")
def home():
    return render_template("home.html")


@app.route("/about")
def about():
    return "<h1>About Page</h1><p>This is some other information.</p>"


@app.route("/customers", methods=["GET", "POST"])
def customers():
    if request.method == "POST":
        first_name = request.form["firstName"]
        last_name = request.form["lastName"]
        phone = request.form["phone"]
        email = request.form["email"]

        col.insert_one(
            {
                "first_name": first_name,
                "last_name": last_name,
                "phone": phone,
                "email": email,
            }
        )
        return redirect("/customers")
    else:
        customers = col.find({})
        return render_template("customers.html", customers_list=list(customers))


if __name__ == "__main__":
    app.run(debug=True)
